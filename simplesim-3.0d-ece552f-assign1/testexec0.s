	.file	1 "testexec.c"

 # GNU C 2.7.2.3 [AL 1.1, MM 40, tma 0.1] SimpleScalar running sstrix compiled by GNU C

 # Cc1 defaults:
 # -mgas -mgpOPT

 # Cc1 arguments (-G value = 8, Cpu = default, ISA = 1):
 # -quiet -dumpbase -O0 -o

gcc2_compiled.:
__gnu_compiled_c:
	.rdata
	.align	2
$LC0:
	.ascii	"Usage: %s <count>\n\000"
	.align	2
$LC1:
	.ascii	"Sum = %d\n\000"
	.text
	.align	2
	.globl	main

	.text

	.loc	1 4
	.ent	main
main:
	.frame	$fp,32,$31		# vars= 8, regs= 2/0, args= 16, extra= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	move	$fp,$sp
	sw	$4,32($fp)
	sw	$5,36($fp)
	jal	__main
	sw	$0,20($fp)
	lw	$2,32($fp)
	li	$3,0x00000002		# 2
	beq	$2,$3,$L2
	lw	$2,36($fp)
	la	$4,$LC0
	lw	$5,0($2)
	jal	printf
	li	$4,0x00000005		# 5
	jal	exit
$L2:
	.set	noreorder
	nop
	.set	reorder
	li	$2,0x00000001		# 1
	sw	$2,16($fp)
$L3:
	lw	$3,36($fp)
	addu	$2,$3,4
	lw	$4,0($2)
	jal	atoi
	lw	$3,16($fp)
	slt	$2,$2,$3
	beq	$2,$0,$L6
	j	$L4
$L6:
	lw	$2,20($fp)
	lw	$3,16($fp)
	addu	$2,$2,$3
	sw	$2,20($fp)
$L5:
	lw	$3,16($fp)
	addu	$2,$3,1
	move	$3,$2
	sw	$3,16($fp)
	j	$L3
$L4:
	la	$4,$LC1
	lw	$5,20($fp)
	jal	printf
	move	$2,$0
	j	$L1
$L1:
	move	$sp,$fp			# sp not trusted here
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addu	$sp,$sp,32
	j	$31
	.end	main
