	.file	1 "testexec.c"

 # GNU C 2.7.2.3 [AL 1.1, MM 40, tma 0.1] SimpleScalar running sstrix compiled by GNU C

 # Cc1 defaults:
 # -mgas -mgpOPT

 # Cc1 arguments (-G value = 8, Cpu = default, ISA = 1):
 # -quiet -dumpbase -O2 -o

gcc2_compiled.:
__gnu_compiled_c:
	.rdata
	.align	2
$LC0:
	.ascii	"Usage: %s <count>\n\000"
	.align	2
$LC1:
	.ascii	"Sum = %d\n\000"
	.text
	.align	2
	.globl	main

	.extern	stdin, 4
	.extern	stdout, 4

	.text

	.loc	1 4
	.ent	main
main:
	.frame	$sp,32,$31		# vars= 0, regs= 4/0, args= 16, extra= 0
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$16,16($sp)
	move	$16,$4
	sw	$18,24($sp)
	move	$18,$5
	sw	$31,28($sp)
	sw	$17,20($sp)
	jal	__main
	move	$17,$0
	li	$2,0x00000002		# 2
	beq	$16,$2,$L14
	.set	noreorder
	lw	$5,0($18)
	.set	reorder
	la	$4,$LC0
	jal	printf
	li	$4,0x00000005		# 5
	jal	exit
$L14:
	li	$16,0x00000001		# 1
$L15:
	.set	noreorder
	lw	$4,4($18)
	.set	reorder
	jal	atoi
	slt	$2,$2,$16
	bne	$2,$0,$L16
	addu	$17,$17,$16
	addu	$16,$16,1
	j	$L15
$L16:
	la	$4,$LC1
	move	$5,$17
	jal	printf
	move	$2,$0
	lw	$31,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	addu	$sp,$sp,32
	j	$31
	.end	main
